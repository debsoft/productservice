package com.deb.productservice.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

@Configuration
@EnableAutoConfiguration
@EnableJpaAuditing
@EnableTransactionManagement
@EntityScan({"com.deb.userservice.model"})
@ComponentScan({"com.deb.userservice.*"})
@EnableJpaRepositories({"com.deb.userservice.repo"})
@SpringBootApplication
@PropertySource("classpath:error_messages.properties")
public class UserserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserserviceApplication.class, args);
	}

	@GetMapping("/hello")
	public ResponseEntity hello() {
		return new ResponseEntity("Hello,Welcome to the microservices world",HttpStatus.OK);
	}
}
