package com.deb.productservice.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.deb.productservice.model.User;

public interface UserRepo extends JpaRepository<User, Long>{

	public User findByUsername(String username);

	public User findByEmail(String email);
}
